# Quality Checkers
This repository contains scripts to statically check code quality of a project.
As new languages are being supported, new checkers will be created.
So start watching this repository to be updated when the things change.

## Golang
```
$ curl https://bitbucket.org/danielfernandessilva/quality-checkers/raw/master/static/golang.sh | bash
```

## Python
```
$ curl https://bitbucket.org/danielfernandessilva/quality-checkers/raw/master/static/python.sh | bash
```
