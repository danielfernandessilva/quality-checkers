#!/usr/bin/env bash

set -e
set -o pipefail

level=$1
if [ -z "$level" ]
then
  echo "Nível de complexidade não foi definido. Deve ser entre 'a' e 'f'."
  exit 1
fi
cyclomatic_complexity_result=$(radon cc -n$level .)
if [ -z "$cyclomatic_complexity_result" ]
then
  echo "Todos os arquivos possuem baixa complexidade ciclomática."
else
  echo "Alguns arquivos foram identificados com complexidade maior ou igual a '$level'."
  printf "$cyclomatic_complexity_result\n"
  exit 1
fi
