#!/usr/bin/env bash

set -e
set -o pipefail

wget -O - -q https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s v1.17.1
./bin/golangci-lint run --enable-all
